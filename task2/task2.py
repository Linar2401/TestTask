import os
import requests

def save_pages(pages: list, path: str):
    abs_path = os.path.abspath(path)

    for index, page in enumerate(pages):
        with open('{}.html'.format(index), 'wb') as file:
            try:
                response = requests.get(page)
                if response.status_code == requests.codes.ok:
                    file.write(response.content)
            except Exception as e:
                continue
