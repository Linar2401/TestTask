def prime_numbers(n:int):
    result = []
    i = 1
    while len(result) < n:
        i += 1
        # пробегаем по списку (lst) простых чисел
        for j in result:
            if i % j == 0:
                break
        else:
            result.append(i)
    return (result)

