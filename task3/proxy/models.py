from django.db import models
from django.utils.html import format_html

from proxy.mixins import NameMixin


# Create your models here.

class FuelStation(NameMixin):
    number = models.IntegerField()
    address = models.CharField(max_length=500)

    coordinates = models.ForeignKey(
        'proxy.Coordinate',
        on_delete=models.CASCADE,
        verbose_name='Координаты',
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = 'Заправочная станция'
        verbose_name_plural = 'Заправочные станции'

    def __str__(self):
        return '{}'.format(self.name)


class Coordinate(models.Model):
    x = models.FloatField()
    y = models.FloatField()

    class Meta:
        verbose_name = 'Координата'
        verbose_name_plural = 'Координаты'

    def __str__(self):
        return '{} {}'.format(self.x, self.y)


class ImageUrls(models.Model):
    url = models.URLField(max_length=200)

    class Meta:
        verbose_name = 'Ссылка на изображение'
        verbose_name_plural = 'Ссылки на изображения'

    @property
    def preview(self):
        return format_html('<img src="{}" style="max-width:200px; max-height:200px"/>'.format(self.url))

    def __str__(self):
        return '{}'.format(self.url)


class FuelStationImage(ImageUrls):
    fuel_station = models.ForeignKey(
        'proxy.FuelStation',
        on_delete=models.CASCADE,
        verbose_name='Заправочная станция',
        null=True,
        blank=True,
        related_name='images'
    )

    class Meta:
        verbose_name = 'Ссылка на изображение АЗС'
        verbose_name_plural = 'Ссылки на изображения АЗС'


class AdditionalServices(NameMixin):
    icon = models.ForeignKey(
        'proxy.ImageUrls',
        on_delete=models.CASCADE,
        verbose_name='Иконка',
        null=True,
        blank=True,
    )

    fuel_station = models.ForeignKey(
        'proxy.FuelStation',
        on_delete=models.CASCADE,
        verbose_name='Заправочная станция',
        null=True,
        blank=True,
        related_name='add_services'
    )

    class Meta:
        verbose_name = 'Дополнительная услуга'
        verbose_name_plural = 'Дополнительные услуги'

    def __str__(self):
        return '{}'.format(self.name)


class FuelPrice(NameMixin):
    fuel_station = models.ForeignKey(
        'proxy.FuelStation',
        on_delete=models.CASCADE,
        verbose_name='Заправочная станция',
        null=True,
        blank=True,
        related_name='fuels'
    )
    price = models.IntegerField(verbose_name='Цена(к.)')
    icon = models.ForeignKey(
        'proxy.ImageUrls',
        on_delete=models.CASCADE,
        verbose_name='Иконка',
        null=True,
        blank=True,
    )
    cur = models.ForeignKey(
        'proxy.Currency',
        on_delete=models.CASCADE,
        verbose_name='Валюта',
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = 'Топливо'
        verbose_name_plural = 'Топливо'

    def __str__(self):
        return '{}'.format(self.name)


class Currency(NameMixin):
    class Meta:
        verbose_name = 'Валюта'
        verbose_name_plural = 'Валюты'

    def __str__(self):
        return '{}'.format(self.name)
