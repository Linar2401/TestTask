from django.contrib import admin
from django.urls import path, include
from proxy.views import FuelStationViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter(trailing_slash=True)
router.register('fuel_station', FuelStationViewSet, basename='fuel_station')

urlpatterns = [
    path('', include(router.urls)),
]
