from django.db import models


class NameMixin(models.Model):
    name = models.CharField(max_length=50)