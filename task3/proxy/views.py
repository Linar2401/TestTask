from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet
from proxy.models import FuelStation
from proxy.serializers import ReadOnlyFuelStationSerializer
from rest_framework.permissions import AllowAny


class FuelStationViewSet(
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    GenericViewSet,
):
    queryset = FuelStation.objects.all()
    serializer_class = ReadOnlyFuelStationSerializer
    permission_classes = (AllowAny,)


