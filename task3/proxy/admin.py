from django.contrib import admin
from django.utils.html import format_html

from proxy.models import (
    AdditionalServices,
    Coordinate,
    FuelPrice,
    ImageUrls,
    Currency,
    FuelStation,
    FuelStationImage,
)


class AdditionalServicesAdmin(admin.ModelAdmin):
    pass


class AdditionalServicesInline(admin.TabularInline):
    model = AdditionalServices
    fk_name = 'fuel_station'


class CoordinateAdmin(admin.ModelAdmin):
    pass


class FuelPriceAdmin(admin.ModelAdmin):
    pass


class FuelPriceInline(admin.TabularInline):
    model = FuelPrice
    fk_name = 'fuel_station'


class ImageUrlsAdmin(admin.ModelAdmin):
    readonly_fields = ['preview']
    list_display = ['preview']

    def preview(self, obj):
        return obj.preview

    preview.short_description = 'Изображение'
    preview.allow_tags = True


class FuelStationImageInline(admin.TabularInline):
    model = FuelStationImage
    fk_name = 'fuel_station'
    readonly_fields = ['preview']
    list_display = ['preview']


class CurrencyAdmin(admin.ModelAdmin):
    pass


class FuelStationAdmin(admin.ModelAdmin):
    # fields = [
    #     'name',
    #     'number',
    #     'address',
    #     'coordinates',
    #     'images',
    #     'add_services',
    #     'fuels',
    # ]
    # list_select_related =
    inlines = [
        FuelStationImageInline,
        AdditionalServicesInline,
        FuelPriceInline
    ]


admin.site.register(FuelStation, FuelStationAdmin)
admin.site.register(AdditionalServices, AdditionalServicesAdmin)
admin.site.register(Coordinate, CoordinateAdmin)
admin.site.register(FuelPrice, FuelPriceAdmin)
admin.site.register(ImageUrls, ImageUrlsAdmin)
admin.site.register(Currency, CurrencyAdmin)
