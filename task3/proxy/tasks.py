from proxy.mock import get_mocked_data
from task3.celery import app
from proxy.serializers import CreateSourceOneFuelStationSerializer, CreateSourceTwoFuelStationSerializer
from proxy.models import (
    FuelStation,
    FuelPrice,
    FuelStationImage,
    AdditionalServices,
    Coordinate,
)


@app.tasks
def get_from_source_one():
    data = get_mocked_data(True)
    serializer = CreateSourceOneFuelStationSerializer(data=data, many=True)
    if serializer.is_valid(raise_exception=True):
        validated_data = serializer.validated_data
        for row in validated_data:
            instance = FuelStation.objects.get_or_create(id=row['id'])
            instance.name = row['name']
            instance.address = row['address']
            instance.number = row['number']
            instance.coordinate = Coordinate.objects.get_or_create(
                x=row['coordinates'].split(', ')[0],
                y=row['coordinates'].split(', ')[1]
            )

            images = []
            for image_url in validated_data['images']:
                images.append(
                    FuelStationImage(
                        url=image_url,
                        fuel_station=instance
                    )
                )
            FuelStationImage.objects.bulk_create(images)

            add_services = []
            for add_service_name in validated_data['add_services']:
                add_services.append(
                    AdditionalServices(
                        url=add_service_name,
                        fuel_station=instance
                    )
                )
            AdditionalServices.objects.bulk_create(add_services)



@app.tasks
def get_from_source_two():
    data = get_mocked_data(False)
    serializer = CreateSourceTwoFuelStationSerializer(data=data, many=True)
    if serializer.is_valid(raise_exception=True):
        validated_data = serializer.validated_data
        for row in validated_data:
            instance = FuelStation.objects.get_or_create(id=row['id'])
            for fuel in validated_data['fuels']:
                if fuel_instance := FuelPrice.objects.filter(
                        fuel_station=instance.id,
                        name=fuel['name'],
                        cur__name=fuel['cur']
                ):
                    fuel_instance.update(
                        price=fuel['price']
                    )
                else:
                    FuelPrice.objects.create(
                        fuel_station=instance.id,
                        price=fuel['price'],
                        name=fuel['name'],
                        cur__name=fuel['cur'],
                    )
