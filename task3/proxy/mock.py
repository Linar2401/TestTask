import json


def get_mocked_data(first_source: bool = True):
    return json.load(open('test_data.json')) if first_source else json.load(open('test_data2.json'))
