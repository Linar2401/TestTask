from rest_framework import serializers

from proxy.models import FuelStation, FuelPrice, Currency, ImageUrls, Coordinate, FuelStationImage


class CreateFuelPriceSerializer(serializers.Serializer):
    price = serializers.IntegerField()
    name = serializers.CharField(max_length=50)
    cur = serializers.CharField(max_length=50)

    def validate(self, data):
        result = super().validate(data)

        if not Currency.objects.filter(name=data['cur']):
            raise serializers.ValidationError('Currency not found')
        return result


class CreateSourceOneFuelStationSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=50)
    number = serializers.IntegerField()
    address = serializers.CharField(max_length=500)

    coordinates = serializers.CharField(max_length=500)
    images = serializers.ListField(
        child=serializers.URLField(max_length=500, min_length=None, allow_blank=True)
    )
    add_services = serializers.ListField(
        child=serializers.CharField(max_length=500, min_length=None, allow_blank=True)
    )


class CreateSourceTwoFuelStationSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    fuels = serializers.ListField(
        child=CreateFuelPriceSerializer()
    )

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        instance = FuelStation(id=id)
        fuels = []
        for fuel in validated_data['fuels']:
            fuels.append(
                FuelPrice(

                )
            )

    def save(self, **kwargs):
        print(kwargs)


class FuelStationImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = FuelStationImage
        fields = (
            'id',
            'url'
        )
        read_only_fields = fields


class CoordinatesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coordinate
        fields = (
            'x',
            'y'
        )
        read_only_fields = fields

class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = (
            'name',
        )
        read_only_fields = fields


class FuelPriceSerializer(serializers.ModelSerializer):
    cur = CurrencySerializer()

    class Meta:
        model = FuelPrice
        fields = (
            'price',
            'icon',
            'cur'
        )
        read_only_fields = fields


class ReadOnlyFuelStationSerializer(serializers.ModelSerializer):
    images = FuelStationImageSerializer()
    coordinates = CoordinatesSerializer()
    fuels = FuelPriceSerializer(many=True)

    class Meta:
        model = FuelStation
        fields = (
            'id',
            'name',
            'number',
            'address',
            'coordinates',
            'images',
            'fuels',
        )
        read_only_fields = fields
